﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using PIC8.Backend.Annotations;

namespace PIC8
{
    public class Memory : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }



        private object _registerLock = new object();
        public ObservableCollection<Register> Register;



        #region ReRouting

        private readonly List<string> _reRoutePreScalePropertyChangedEvents = new List<string> { "Timer0PrescalerFlag2", "Timer0PrescalerFlag1", "Timer0PrescalerFlag0" };

        private void _reRouteStatusPropertyChangedEvents(object s, PropertyChangedEventArgs e) { new List<string> { "IndirectRegisterBankSelectFlag", "DirectRegisterBankSelect1Flag", "DirectRegisterBankSelect0Flag",
                                                                                                                    "TimeOutFlag", "PowerDownFlag", "ZeroFlag", "DigitCarryFlag", "CarryFlag"}.ForEach(OnPropertyChanged); }

        private void _reRouteOptionPropertyChangedEvents(object s, PropertyChangedEventArgs e) { new List<string> { "RBPullUpEnabledFlag", "InterruptEdgeSelectFlag", "Timer0ClockSourceSelectFlag",
                                                                                                                    "Timer0SourceEdgeSelectFlag", "Timer0PrescalerEnabledFlag", "Timer0PrescalerFlag2",
                                                                                                                    "Timer0PrescalerFlag1", "Timer0PrescalerFlag0" }.ForEach(OnPropertyChanged); }

        private void _reRouteInterruptPropertyChangedEvents(object s, PropertyChangedEventArgs e) { new List<string> { "GlobalInterruptEnabledFlag", "EEWriteCompleteInterruptEnabledFlag",
                                                                                                                       "Timer0OverflowInterruptEnabledFlag", "RB0InterruptEnabledFlag", "RBChangeInterruptEnabledFlag",
                                                                                                                       "Timer0OverflowInterruptFlag", "RB0InterruptFlag", "RPChangeInterruptFlag" }.ForEach(OnPropertyChanged); }

        private void _reRouteTrisPortBPropertyChangedEvents(object s, PropertyChangedEventArgs e) { new List<string> { "TrisPortB0", "TrisPortB1", "TrisPortB2", "TrisPortB3", "TrisPortB4", 
                                                                                                                       "TrisPortB5", "TrisPortB6", "TrisPortB7" }.ForEach(OnPropertyChanged); }

        private void _reRouteDataPortBPropertyChangedEvents(object s, PropertyChangedEventArgs e) { new List<string> { "DataPortB0", "DataPortB1", "DataPortB2", "DataPortB3", "DataPortB4",
                                                                                                                       "DataPortB5", "DataPortB6", "DataPortB7" }.ForEach(OnPropertyChanged); }

        private void _reRouteTrisPortAPropertyChangedEvents(object s, PropertyChangedEventArgs e) { new List<string> { "TrisPortA0", "TrisPortA1", "TrisPortA2", "TrisPortA3", "TrisPortA4", 
                                                                                                                       "TrisPortA5", "TrisPortA6", "TrisPortA7" }.ForEach(OnPropertyChanged); }
        private void _reRouteDataPortAPropertyChangedEvents(object s, PropertyChangedEventArgs e) { new List<string> { "DataPortA0", "DataPortA1", "DataPortA2", "DataPortA3", "DataPortA4", 
                                                                                                                       "DataPortA5", "DataPortA6", "DataPortA7" }.ForEach(OnPropertyChanged); }
        #endregion



        public Memory()
        {
            Register = new ObservableCollection<Register>();
            BindingOperations.EnableCollectionSynchronization(Register, _registerLock); //Async Collection Change
            Init();

            PropertyChanged += (o, a) => { if (_reRoutePreScalePropertyChangedEvents.All(name => name != a.PropertyName)) return;

                                                                   OnPropertyChanged(nameof(WatchDogTimerRate));
                                                                   OnPropertyChanged(nameof(Timer0Rate));
                                                                   OnPropertyChanged(nameof(Timer0RateAsString));
                                                                   OnPropertyChanged(nameof(WDTRateAsString)); };

        }


        public void Reset()
        {
            lock (_registerLock)
            {
                //Unregister PropertChanged ReRoutes
                Register[0x03].PropertyChanged -= _reRouteStatusPropertyChangedEvents;
                Register[0x05].PropertyChanged -= _reRouteDataPortAPropertyChangedEvents;
                Register[0x06].PropertyChanged -= _reRouteDataPortBPropertyChangedEvents;
                Register[0x81].PropertyChanged -= _reRouteOptionPropertyChangedEvents;
                Register[0x85].PropertyChanged -= _reRouteTrisPortAPropertyChangedEvents;
                Register[0x86].PropertyChanged -= _reRouteTrisPortBPropertyChangedEvents;
                Register[0x0B].PropertyChanged -= _reRouteInterruptPropertyChangedEvents;
                Register.Clear();
            }
            Init();
            ProgramCounter = 0;
        }

        private void Init()
        {
            Random random = new Random();

            lock (_registerLock)
            {
                for (int i = 0; i < 0x0C; i++)
                {
                    Register.Add(new Register((byte)i));
                }

                //Init registers for Bank 0 as described in documentation
                Register[0x00].Value = 0b0000_0000;
                Register[0x01].Value = (byte)random.Next(0b0000_0000, 0b1111_1111);
                Register[0x02].Value = 0b0000_0000;
                Register[0x03].Value = (byte)random.Next(0b0001_1000, 0b0001_1111);
                Register[0x04].Value = 0b0000_0000;
                Register[0x05].Value = (byte)random.Next(0b0000_0000, 0b0001_1111);
                Register[0x06].Value = (byte)random.Next(0b0000_0000, 0b1111_1111);
                Register[0x07].Value = 0b0000_0000;
                Register[0x08].Value = (byte)random.Next(0b0000_0000, 0b1111_1111);
                Register[0x09].Value = (byte)random.Next(0b0000_0000, 0b1111_1111);
                Register[0x0A].Value = 0b0000_0000;
                Register[0x0B].Value = (byte)random.Next(0b0000_0000, 0b0000_0001);

                for (int i = 0; i < 68; i++) //68 General Purpose Register
                {
                    Register.Add(new Register((byte)(0x0C + i)));
                    Register[0x0C + i].Value = (byte)random.Next(0b0000_0000, 0b1111_1111);
                }

                //Init dummy Register for Offset
                for (int i = 0; i < (128 - 68 - 0x0C); i++)
                {
                    Register.Add(new Register((byte)(0x0C + 68 + i)));
                    Register[0x0C + 68 + i].Value = 0b1111_1111;
                }



                //Init registers for Bank 1 as described in documentation
                Register.Add(Register[0x00]); //0x80

                Register.Add(new Register(0x81));
                Register[0x81].Value = 0b1111_1111;

                Register.Add(Register[0x02]); //0x82
                Register.Add(Register[0x03]); //0x83
                Register.Add(Register[0x04]); //0x84

                Register.Add(new Register(0x85));
                Register[0x85].Value = 0b0001_1111;

                Register.Add(new Register(0x86));
                Register[0x86].Value = 0b1111_1111;

                Register.Add(new Register(0x87));
                Register[0x87].Value = 0b0000_0000;

                Register.Add(new Register(0x88));
                Register[0x88].Value = (byte)random.Next(0b0000_0000, 0b0000_1000);

                Register.Add(new Register(0x89));
                Register[0x89].Value = 0b0000_0000;

                Register.Add(Register[0x0A]); //0x8A
                Register.Add(Register[0x0B]); //0x8B

                for (int i = 0; i < 68; i++)
                {
                    Register.Add(Register[0x0C + i]);
                }

                for (int i = 0; i < (128 - 68 - 0x0B); i++)
                {
                    Register.Add(new Register((byte)(0x8C + 68 + i)));
                    Register[0x8C + 68 + i].Value = 0b1111_1111;
                }

                //Register PropertChanged ReRoutes
                Register[0x03].PropertyChanged += _reRouteStatusPropertyChangedEvents;
                Register[0x05].PropertyChanged += _reRouteDataPortAPropertyChangedEvents;
                Register[0x06].PropertyChanged += _reRouteDataPortBPropertyChangedEvents;
                Register[0x81].PropertyChanged += _reRouteOptionPropertyChangedEvents;
                Register[0x85].PropertyChanged += _reRouteTrisPortAPropertyChangedEvents;
                Register[0x86].PropertyChanged += _reRouteTrisPortBPropertyChangedEvents;
                Register[0x0B].PropertyChanged += _reRouteInterruptPropertyChangedEvents;

            }

            OnPropertyChanged(""); //Trigger for all Properties
        }

        private int _programCounter;

        public int ProgramCounter
        {
            get => _programCounter;
            set
            {
                HandleTimer();
                HandleWatchDog();

                //Update UI-Property
                if (_programCounter != value)
                {
                    // Update Binding of old pc if there is element in collection, otherwise GC will clean assignment
                    MicroController.AssemblerProgram.Source.Where(pm => pm.MemoryAddress == _programCounter)
                        .DefaultIfEmpty(new CommandLine()).Single().IsCurrentExecution = false;

                    // Update Binding of new pc if there is element in collection, otherwise GC will clean assignment
                    MicroController.AssemblerProgram.Source.Where(pm => pm.MemoryAddress == value)
                        .DefaultIfEmpty(new CommandLine()).Single().IsCurrentExecution = true;
                }
                
                //Limit to 13 bits
                value &= 0b1_1111_1111_1111;
                _programCounter = value;

                Register[0x02].Value = (byte) (value & 0b1111_1111);
                OnPropertyChanged();

                if(!MicroController.Status.TimerZyklusHandled) 
                    { MicroController.Status.IncreaseRunTime(); }

                MicroController.Status.TimerZyklusHandled = true;
            }
        }

        public int WatchDogTimerRate => (Register[0x81].GetBit(2) ? 16 : 1)
                                        * (Register[0x81].GetBit(1) ? 4 : 1)
                                        * (Register[0x81].GetBit(0) ? 2 : 1);

        public string WDTRateAsString => $"1:{WatchDogTimerRate}";

        public int Timer0Rate => WatchDogTimerRate * 2;

        public string Timer0RateAsString => $"1:{Timer0Rate}";



        private int _preScaler;

        public int PreScaler
        {
            get => _preScaler;
            set
            {
                _preScaler = value & 0b1111_1111;
                OnPropertyChanged();
            }
        }

        private void HandleTimer()
        {
            if (MicroController.Status.TimerZyklusHandled) return; // falls pc change nicht neuen zyklus verursacht

            if (Timer0ClockSourceSelectFlag) return; // wenn false dann weiter

            if (Timer0PrescalerEnabledFlag) return; //wenn false dann weiter

            PreScalerTick(Timer0Rate);

            //Auf Overflow prüfen
            if (PreScaler == 0 && Register[0x01].Value == 0)
                { Timer0OverflowInterruptFlag = true; }
        }

        internal void PreScalerTick(int rate)
        {
            if ((PreScaler + (256 / rate)) >= 256) //Overflow
            {
                if (Timer0PrescalerEnabledFlag)
                    { MicroController.Status.WatchDogTicks++; } //WDT
                else
                    { Register[0x01].Value++; } //Timer
            }

            PreScaler += (256 / rate); //Auto-Overflow
        }

        private void HandleWatchDog()
        {
            if (MicroController.Status.TimerZyklusHandled) return; // falls pc change nicht neuen zyklus verursacht

            if (!MicroController.Status.IsWatchDogEnabled) return;

            if (Timer0PrescalerEnabledFlag)
                { PreScalerTick(WatchDogTimerRate); } //PSA = 1 => preScaler für WDT
            else
                { MicroController.Status.WatchDogTicks++; }


            if ((!Timer0PrescalerEnabledFlag || (Timer0PrescalerEnabledFlag && PreScaler == 0)) && MicroController.Status.WatchDogTicks == 0) //PreScaler muss nur 0 sein, wenn dem WDT zugeordnet (PSA)
            {
                MicroController.Status.TimeOutHandleRequired = true;
            }
        }



        #region PortA

        public Register TrisPortA
        {
            get => Register[0x85];
            set
            {
                Register[0x85] = value;
                OnPropertyChanged();
            }
        }

        public bool TrisPortA0
        {
            get => TrisPortA.GetBit(0);
            set
            {
                TrisPortA.SetBit(0, value);
                OnPropertyChanged();
            }
        }

        public bool TrisPortA1
        {
            get => TrisPortA.GetBit(1);
            set
            {
                TrisPortA.SetBit(1, value);
                OnPropertyChanged();
            }
        }

        public bool TrisPortA2
        {
            get => TrisPortA.GetBit(2);
            set
            {
                TrisPortA.SetBit(2, value);
                OnPropertyChanged();
            }
        }

        public bool TrisPortA3
        {
            get => TrisPortA.GetBit(3);
            set
            {
                TrisPortA.SetBit(3, value);
                OnPropertyChanged();
            }
        }

        public bool TrisPortA4
        {
            get => TrisPortA.GetBit(4);
            set
            {
                TrisPortA.SetBit(4, value);
                OnPropertyChanged();
            }
        }

        public Register DataPortA
        {
            get => Register[0x05];
            set
            {
                Register[0x05] = value;
                OnPropertyChanged();
            }
        }

        public bool DataPortA0
        {
            get => DataPortA.GetBit(0);
            set
            {
                DataPortA.SetBit(0, value);
                OnPropertyChanged();
            }
        }

        public bool DataPortA1
        {
            get => DataPortA.GetBit(1);
            set
            {
                DataPortA.SetBit(1, value);
                OnPropertyChanged();
            }
        }

        public bool DataPortA2
        {
            get => DataPortA.GetBit(2);
            set
            {
                DataPortA.SetBit(2, value);
                OnPropertyChanged();
            }
        }

        public bool DataPortA3
        {
            get => DataPortA.GetBit(3);
            set
            {
                DataPortA.SetBit(3, value);
                OnPropertyChanged();
            }
        }

        public bool DataPortA4
        {
            get => DataPortA.GetBit(4);
            set
            {
                DataPortA.SetBit(4, value);
                OnPropertyChanged();
            }
        }

        public bool DataPortA5
        {
            get => DataPortA.GetBit(5);
            set
            {
                DataPortA.SetBit(5, value);
                OnPropertyChanged();
            }
        }

        #endregion

        #region PortB

        public Register TrisPortB
        {
            get => Register[0x86];
            set
            {
                Register[0x86] = value;
                OnPropertyChanged();
            }
        }

        public bool TrisPortB0
        {
            get => TrisPortB.GetBit(0);
            set
            {
                TrisPortB.SetBit(0, value);
                OnPropertyChanged();
            }
        }

        public bool TrisPortB1
        {
            get => TrisPortB.GetBit(1);
            set
            {
                TrisPortB.SetBit(1, value);
                OnPropertyChanged();
            }
        }

        public bool TrisPortB2
        {
            get => TrisPortB.GetBit(2);
            set
            {
                TrisPortB.SetBit(2, value);
                OnPropertyChanged();
            }
        }

        public bool TrisPortB3
        {
            get => TrisPortB.GetBit(3);
            set
            {
                TrisPortB.SetBit(3, value);
                OnPropertyChanged();
            }
        }

        public bool TrisPortB4
        {
            get => TrisPortB.GetBit(4);
            set
            {
                TrisPortB.SetBit(4, value);
                OnPropertyChanged();
            }
        }

        public bool TrisPortB5
        {
            get => TrisPortB.GetBit(5);
            set
            {
                TrisPortB.SetBit(5, value);
                OnPropertyChanged();
            }
        }

        public bool TrisPortB6
        {
            get => TrisPortB.GetBit(6);
            set
            {
                TrisPortB.SetBit(6, value);
                OnPropertyChanged();
            }
        }

        public bool TrisPortB7
        {
            get => TrisPortB.GetBit(7);
            set
            {
                TrisPortB.SetBit(7, value);
                OnPropertyChanged();
            }
        }

        public Register DataPortB
        {
            get => Register[0x06];
            set
            {
                Register[0x06] = value;
                OnPropertyChanged();
            }
        }

        public bool DataPortB0
        {
            get => DataPortB.GetBit(0);
            set
            {
                DataPortB.SetBit(0, value);
                OnPropertyChanged();
            }
        }

        public bool DataPortB1
        {
            get => DataPortB.GetBit(1);
            set
            {
                DataPortB.SetBit(1, value);
                OnPropertyChanged();
            }
        }

        public bool DataPortB2
        {
            get => DataPortB.GetBit(2);
            set
            {
                DataPortB.SetBit(2, value);
                OnPropertyChanged();
            }
        }

        public bool DataPortB3
        {
            get => DataPortB.GetBit(3);
            set
            {
                DataPortB.SetBit(3, value);
                OnPropertyChanged();
            }
        }

        public bool DataPortB4
        {
            get => DataPortB.GetBit(4);
            set
            {
                DataPortB.SetBit(4, value);
                OnPropertyChanged();
            }
        }

        public bool DataPortB5
        {
            get => DataPortB.GetBit(5);
            set
            {
                DataPortB.SetBit(5, value);
                OnPropertyChanged();
            }
        }

        public bool DataPortB6
        {
            get => DataPortB.GetBit(6);
            set
            {
                DataPortB.SetBit(6, value);
                OnPropertyChanged();
            }
        }

        public bool DataPortB7
        {
            get => DataPortB.GetBit(7);
            set
            {
                DataPortB.SetBit(7, value);
                OnPropertyChanged();
            }
        }


        #endregion

        #region Status Register Bits

        public Register Status
        {
            get => Register[0x03];
            set
            {
                Register[0x03] = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Status 7 - IRP
        /// </summary>
        public bool IndirectRegisterBankSelectFlag
        {
            get => Status.GetBit(7); //IndirectRegisterBankSelectFlag-Index: 7
            set
            {
                Status.SetBit(7, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Status 6 - RP1
        /// </summary>
        public bool DirectRegisterBankSelect1Flag
        {
            get => Status.GetBit(6); //DirectRegisterBankSelect1Flag-Index: 6
            set
            {
                Status.SetBit(6, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Status 5 - RP0
        /// </summary>
        public bool DirectRegisterBankSelect0Flag
        {
            get => Status.GetBit(5); //DirectRegisterBankSelect0Flag-Index: 5
            set
            {
                Status.SetBit(5, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Status 4 - TO
        /// </summary>
        public bool TimeOutFlag
        {
            get => Status.GetBit(4); //TimeOutFlag-Index: 4
            set
            {
                Status.SetBit(4, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Status 3 - PD
        /// </summary>
        public bool PowerDownFlag
        {
            get => Status.GetBit(3); //PowerDownFlag-Index: 3
            set
            {
                Status.SetBit(3, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Status 2 - Z
        /// </summary>
        public bool ZeroFlag
        {
            get => Status.GetBit(2); //ZeroFlag-Index: 2
            set
            {
                Status.SetBit(2, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Status 1 - DC
        /// </summary>
        public bool DigitCarryFlag
        {
            get => Status.GetBit(1); //DigitCarryFlag-Index: 1
            set
            {
                Status.SetBit(1, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Status 0 - C
        /// </summary>
        public bool CarryFlag
        {
            get => Status.GetBit(0); //CarryFlag-Index: 0
            set
            {
                Status.SetBit(0, value);
                OnPropertyChanged();
            }
        }

        #endregion

        #region Option Register Bits

        public Register Option
        {
            get => Register[0x81];
            set
            {
                Register[0x81] = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Option 7 - RBPU
        /// </summary>
        public bool RBPullUpEnabledFlag
        {
            get => Option.GetBit(7); //RBPullUpEnabledFlag-Index: 7
            set
            {
                Option.SetBit(7, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Option 6 - INTEDG
        /// </summary>
        public bool InterruptEdgeSelectFlag
        {
            get => Option.GetBit(6); //InterruptEdgeSelectFlag-Index: 6
            set
            {
                Option.SetBit(6, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Option 5 - T0CS
        /// </summary>
        public bool Timer0ClockSourceSelectFlag
        {
            get => Option.GetBit(5); //Timer0ClockSourceSelectFlag-Index: 5
            set
            {
                Option.SetBit(5, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Option 4 - T0SE
        /// </summary>
        public bool Timer0SourceEdgeSelectFlag
        {
            get => Option.GetBit(4); //Timer0SourceEdgeSelectFlag-Index: 4
            set
            {
                Option.SetBit(4, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Option 3 - PSA
        /// </summary>
        public bool Timer0PrescalerEnabledFlag
        {
            get => Option.GetBit(3); //Timer0PrescalerEnabledFlag-Index: 3
            set
            {
                Option.SetBit(3, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Option 2 - PS2
        /// </summary>
        public bool Timer0PrescalerFlag2
        {
            get => Option.GetBit(2); //Timer0PrescalerFlag2-Index: 2
            set
            {
                Option.SetBit(2, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Option 1 - PS1
        /// </summary>
        public bool Timer0PrescalerFlag1
        {
            get => Option.GetBit(1); //Timer0PrescalerFlag1-Index: 1
            set
            {
                Option.SetBit(1, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Option 0 - PS0
        /// </summary>
        public bool Timer0PrescalerFlag0
        {
            get => Option.GetBit(0); //Timer0PrescalerFlag0-Index: 0
            set
            {
                Option.SetBit(0, value);
                OnPropertyChanged();
            }
        }

        #endregion

        #region Interrupt Bits

        public Register Interrupt
        {
            get => Register[0x0B];
            set
            {
                Register[0x0B] = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Interrupt 7 - GIE
        /// </summary>
        public bool GlobalInterruptEnabledFlag
        {
            get => Interrupt.GetBit(7); //GlobalInterruptEnabledFlag-Index: 7
            set
            {
                Interrupt.SetBit(7, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Interrupt 6 - EIE
        /// </summary>
        public bool EEWriteCompleteInterruptEnabledFlag
        {
            get => Interrupt.GetBit(6); //EEWriteCompleteInterruptEnabledFlag-Index: 6
            set
            {
                Interrupt.SetBit(6, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Interrupt 5 - T0IE
        /// </summary>
        public bool Timer0OverflowInterruptEnabledFlag
        {
            get => Interrupt.GetBit(5); //Timer0OverflowInterruptEnabledFlag-Index: 5
            set
            {
                Interrupt.SetBit(5, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Interrupt 4 - INTE
        /// </summary>
        public bool RB0InterruptEnabledFlag
        {
            get => Interrupt.GetBit(4); //RB0InterruptEnabledFlag-Index: 4
            set
            {
                Interrupt.SetBit(4, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Interrupt 3 - RBIE
        /// </summary>
        public bool RBChangeInterruptEnabledFlag
        {
            get => Interrupt.GetBit(3); //RBChangeInterruptEnabledFlag-Index: 3
            set
            {
                Interrupt.SetBit(3, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Interrupt 2 - T0IF
        /// </summary>
        public bool Timer0OverflowInterruptFlag
        {
            get => Interrupt.GetBit(2); //Timer0OverflowInterruptFlag-Index: 2
            set
            {
                Interrupt.SetBit(2, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Interrupt 1 - INTF
        /// </summary>
        public bool RB0InterruptFlag
        {
            get => Interrupt.GetBit(1); //RB0InterruptFlag-Index: 1
            set
            {
                Interrupt.SetBit(1, value);
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Interrupt 0 - RBIF
        /// </summary>
        public bool RPChangeInterruptFlag
        {
            get => Interrupt.GetBit(0); //RPChangeInterruptFlag-Index: 0
            set
            {
                Interrupt.SetBit(0, value);
                OnPropertyChanged();
            }
        }

        #endregion


    }
}