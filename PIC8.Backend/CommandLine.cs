﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using PIC8.Backend.Annotations;

namespace PIC8
{
    public class CommandLine : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        { PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName)); }



        public bool IsExecutable { get; internal set; }



        private int _memoryAddress;

        public int MemoryAddress
        {
            get => _memoryAddress;
            set
            {
                _memoryAddress = value; 
                OnPropertyChanged();
            }
        }



        private bool _isCurrentExecution;

        public bool IsCurrentExecution
        {
            get => _isCurrentExecution;
            set
            {
                _isCurrentExecution = value;
                OnPropertyChanged();
            }
        }



        private int _command;

        public int Command
        {
            get => _command;
            set
            {
                _command = value; 
                OnPropertyChanged();
            }
        }



        private bool _isBreakpoint;

        public bool IsBreakPoint
        {
            get => _isBreakpoint;
            set
            {
                _isBreakpoint = value; 
                OnPropertyChanged();
            }
        }

        
        
        private string _text;
        
        public string Text
        {
            get => _text;
            set 
            { 
                _text = value; 
                OnPropertyChanged();
            }
        }



        private int _lineNumber;

        public int LineNumber
        {
            get => _lineNumber;
            set 
            { 
                _lineNumber = value; 
                OnPropertyChanged();
            }
        }
    }
}