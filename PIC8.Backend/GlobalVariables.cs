﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PIC8
{
    public static class GlobalVariables
    {
        public static readonly UInt16 EightBitBoundary = 256;
        public static readonly UInt16 FourBitBoundary = 16;
        public static readonly UInt16 MemorySize = 256;
    }
}
