﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PIC8
{
    public static class Instructions
    {
        private static readonly List<int> NOPCases = new List<int> { 0b00_0000_0000_0000, 0b00_0000_0010_0000, 0b00_0000_0100_0000, 0b00_0000_0110_0000 };

        public static void ParseCommand(CommandLine cmd)
        {
            cmd.IsCurrentExecution = true;
            MicroController.Status.TimerZyklusHandled = false;
            MicroController.Memory.ProgramCounter++;

            switch (cmd.Command)
            {
                // Handle ("special") Commands without Parameter
                case 0x0064: Command_CLRWDT(); return;
                case 0x0009: Command_RETFIE(); return; //DONE
                case 0x0008: Command_RETURN(); break; //DONE
                case 0x063: Command_SLEEP(); return;

                //DONE P1 - P11
                //TODO EEProm (Pascal - Robin P12)
                //DONE P13 - P14
                //DONE 101



                //BYTE - ORIENTED FILE REGISTER OPERATIONS
                case int n when (n >= 0b00_0111_0000_0000 && n < 0b00_0111_1111_1111): Command_ADDWF(cmd.Command); break; //DONE
                case int n when (n >= 0b00_0101_0000_0000 && n < 0b00_0101_1111_1111): Command_ANDWF(cmd.Command); break; //DONE
                case int n when (n >= 0b00_0001_1000_0000 && n < 0b00_0001_1111_1111): Command_CLRF(cmd.Command); break; //DONE
                case int n when (n >= 0b00_0001_0000_0000 && n < 0b00_0001_0111_1111): Command_CLRW(cmd.Command); break; //DONE
                case int n when (n >= 0b00_1001_0000_0000 && n < 0b00_1001_1111_1111): Command_COMF(cmd.Command); break; //DONE
                case int n when (n >= 0b00_0011_0000_0000 && n < 0b00_0011_1111_1111): Command_DECF(cmd.Command); break; //DONE
                case int n when (n >= 0b00_1011_0000_0000 && n < 0b00_1011_1111_1111): Command_DECFSZ(cmd.Command); break; //DONE
                case int n when (n >= 0b00_1010_0000_0000 && n < 0b00_1010_1111_1111): Command_INCF(cmd.Command); break; //DONE
                case int n when (n >= 0b00_1111_0000_0000 && n < 0b00_1111_1111_1111): Command_INCFSZ(cmd.Command); break; //DONE
                case int n when (n >= 0b00_0100_0000_0000 && n < 0b00_0100_1111_1111): Command_IORWF(cmd.Command); break; //DONE
                case int n when (n >= 0b00_1000_0000_0000 && n < 0b00_1000_1111_1111): Command_MOVF(cmd.Command); break; //DONE
                case int n when (n >= 0b00_0000_1000_0000 && n < 0b00_0000_1111_1111): Command_MOVWF(cmd.Command); break; //DONE
                case int n when NOPCases.Any(c => c == n): Command_NOP(); break; //DONE
                case int n when (n >= 0b00_1101_0000_0000 && n < 0b00_1101_1111_1111): Command_RLF(cmd.Command); break; //DONE
                case int n when (n >= 0b00_1100_0000_0000 && n < 0b00_1100_1111_1111): Command_RRF(cmd.Command); break; //DONE
                case int n when (n >= 0b00_0010_0000_0000 && n < 0b00_0010_1111_1111): Command_SUBWF(cmd.Command); break; //DONE
                case int n when (n >= 0b00_1110_0000_0000 && n < 0b00_1110_1111_1111): Command_SWAPF(cmd.Command); break; //DONE
                case int n when (n >= 0b00_0110_0000_0000 && n < 0b00_0110_1111_1111): Command_XORWF(cmd.Command); break; //DONE

                //BIT - ORIENTED FILE REGISTER OPERATIONS
                case int n when (n >= 0b01_0000_0000_0000 && n < 0b01_0011_1111_1111): Command_BCF(cmd.Command); break; //DONE
                case int n when (n >= 0b01_0100_0000_0000 && n < 0b01_0111_1111_1111): Command_BSF(cmd.Command); break; //DONE
                case int n when (n >= 0b01_1000_0000_0000 && n < 0b01_1011_1111_1111): Command_BTFSC(cmd.Command); break; //DONE
                case int n when (n >= 0b01_1100_0000_0000 && n < 0b01_1111_1111_1111): Command_BTFSS(cmd.Command); break; //DONE

                //LITERAL AND CONTROL OPERATIONS                                                     
                case int n when (n >= 0b11_1110_0000_0000 && n < 0b11_1111_1111_1111): Command_ADDLW(cmd.Command); break; //DONE
                case int n when (n >= 0b11_1001_0000_0000 && n < 0b11_1001_1111_1111): Command_ANDLW(cmd.Command); break; //DONE
                case int n when (n >= 0b10_0000_0000_0000 && n < 0b10_0111_1111_1111): Command_CALL(cmd.Command); break; //DONE
                case int n when (n >= 0b10_1000_0000_0000 && n < 0b10_1111_1111_1111): Command_GOTO(cmd.Command); break; //DONE
                case int n when (n >= 0b11_1000_0000_0000 && n < 0b11_1000_1111_1111): Command_IORLW(cmd.Command); break; //DONE
                case int n when (n >= 0b11_0000_0000_0000 && n < 0b11_0011_1111_1111): Command_MOVLW(cmd.Command); break; //DONE
                case int n when (n >= 0b11_0100_0000_0000 && n < 0b11_0111_1111_1111): Command_RETLW(cmd.Command); break; //DONE
                case int n when (n >= 0b11_1100_0000_0000 && n < 0b11_1101_1111_1111): Command_SUBLW(cmd.Command); break; //DONE
                case int n when (n >= 0b11_1010_0000_0000 && n < 0b11_1010_1111_1111): Command_XORLW(cmd.Command); break; //DONE
            }
        }

        #region Subcommands

        private static void SubCommandSaveResult(bool destinationBit, byte address, byte value)
        {
            if (!destinationBit)
            {
                MicroController.WRegister.Value = value;
                return;
            }

            MicroController.Memory.Register[address].Marked = true;

            if (address == 0x00 || address == 0x80) //Indirect Adressing
            {
                MicroController.Memory.Register[MicroController.Memory.Register[0x04].Value].Value = value;
                return;
            }



            if (address == 0x02 || address == 0x82) //PCL manipulation
            {
                //Unteren 8 bit sind in value enthalten
                int newProgramCounter = (MicroController.Memory.Register[0x0A].Value & 0b0001_1111) << 8;

                newProgramCounter += value;


                MicroController.Status.TimerZyklusHandled = false;
                MicroController.Memory.ProgramCounter = newProgramCounter;

                return;
            }

            if (address == 0x01 || address == 0x81) //Timer neu beschreiben => prescale leeren
            {
                MicroController.Memory.PreScaler = 0;
                MicroController.Memory.Register[address].Value = value;

                return;
            }

            MicroController.Memory.Register[address].Value = value;
        }

        private static void SubCommandUpdateZeroFlag(byte value)
        {
            if (value == 0) { MicroController.Memory.ZeroFlag = true; }
            else { MicroController.Memory.ZeroFlag = false; }
        }
        
        #endregion



        #region Byte-Oriented File Register Operations

        /// <summary>
        /// Add W and f
        /// </summary>
        public static void Command_ADDWF(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            //Add
            int result = MicroController.WRegister.Value + MicroController.Memory.Register[address].Value;

            //Check Carry
            if(result >= GlobalVariables.EightBitBoundary)
            {
                result = (result % GlobalVariables.EightBitBoundary);
                MicroController.Memory.CarryFlag = true;
            }
            else
            { MicroController.Memory.CarryFlag = false; }

            //Check DigitCarry
            int digitResult = MicroController.WRegister.LowerNipple + MicroController.Memory.Register[address].LowerNipple;
            MicroController.Memory.DigitCarryFlag = (digitResult >= GlobalVariables.FourBitBoundary);

            SubCommandUpdateZeroFlag((byte) result);
            SubCommandSaveResult(destinationBit, (byte) address, (byte) result);
        }

        /// <summary>
        /// AND W with f
        /// </summary>
        public static void Command_ANDWF(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            //AND
            int result = MicroController.WRegister.Value & MicroController.Memory.Register[address].Value;

            //SubCommand Handling ZF and result saving
            SubCommandUpdateZeroFlag((byte) result);
            SubCommandSaveResult(destinationBit, (byte) address, (byte) result);
        }



        /// <summary>
        /// Clear f
        /// </summary>
        public static void Command_CLRF(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            SubCommandUpdateZeroFlag(0x00); //Always true
            SubCommandSaveResult(destinationBit, (byte) address, 0x00);
        }


        public static void Command_CLRW(int command)
        {
            Command_CLRF(command);
        }

        /// <summary>
        /// Complement f
        /// </summary>
        public static void Command_COMF(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            int result = 0xff - MicroController.Memory.Register[address].Value;

            //SubCommand Handling ZF and result saving
            SubCommandUpdateZeroFlag((byte) result);
            SubCommandSaveResult(destinationBit, (byte) address, (byte) result);

            
        }

        /// <summary>
        /// Decrement f
        /// </summary>
        public static void Command_DECF(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            int result = MicroController.Memory.Register[address].Value - 1;

            if (result < 0)
            {
                result =  GlobalVariables.EightBitBoundary - Math.Abs(result % GlobalVariables.EightBitBoundary);
            }

            SubCommandUpdateZeroFlag((byte) result);
            SubCommandSaveResult(destinationBit, (byte) address, (byte) result);
        }


        
        /// <summary>
        /// Decrement f, Skip if 0
        /// </summary>
        public static void Command_DECFSZ(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            int result = MicroController.Memory.Register[address].Value - 1;

            if (result < 0)
            {
                result = GlobalVariables.EightBitBoundary - Math.Abs(result % GlobalVariables.EightBitBoundary);
            }

            SubCommandSaveResult(destinationBit, (byte)address, (byte)result);



            if (result != 0) return;

            MicroController.Status.TimerZyklusHandled = false;
            MicroController.Memory.ProgramCounter++;
        }

        /// <summary>
        /// Increment f
        /// </summary>
        public static void Command_INCF(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            int result = MicroController.Memory.Register[address].Value + 1;

            result %= GlobalVariables.EightBitBoundary;

            SubCommandUpdateZeroFlag((byte) result);
            SubCommandSaveResult(destinationBit, (byte) address, (byte) result);
        }

        /// <summary>
        /// Increment f, Skip if 0
        /// </summary>
        public static void Command_INCFSZ(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            int result = MicroController.Memory.Register[address].Value + 1;
            result %= GlobalVariables.EightBitBoundary;

            SubCommandSaveResult(destinationBit, (byte)address, (byte)result);



            if (result != 0) return;

            MicroController.Status.TimerZyklusHandled = false;
            MicroController.Memory.ProgramCounter++;
        }

        /// <summary>
        /// Inclusive OR W with f
        /// </summary>
        public static void Command_IORWF(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            //Add
            int result = MicroController.WRegister.Value | MicroController.Memory.Register[address].Value;

            //SubCommand Handling ZF and result saving
            SubCommandUpdateZeroFlag((byte) result);
            SubCommandSaveResult(destinationBit, (byte) address, (byte) result);
        }

        /// <summary>
        /// Move f
        /// </summary>
        public static void Command_MOVF(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            //Add
            UInt16 value = MicroController.Memory.Register[address].Value;

            //SubCommand Handling ZF and result saving
            SubCommandUpdateZeroFlag((byte) value);
            SubCommandSaveResult(destinationBit, (byte) address, (byte) value);
        }

        public static void Command_MOVWF(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            int value = MicroController.WRegister.Value;

            SubCommandSaveResult(destinationBit, (byte) address, (byte) value);
            SubCommandUpdateZeroFlag((byte) value);
        }
        
        /// <summary>
        /// No Operation
        /// </summary>
        public static void Command_NOP()
        {
            //PC wird vorher erhöht
            // ReSharper disable once RedundantJumpStatement
            return;
        }

        /// <summary>
        /// Rotate Left f through Carry
        /// </summary>
        public static void Command_RLF(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            //Rotate
            UInt16 value = (ushort) (MicroController.Memory.Register[address].Value << 1);

            if(MicroController.Memory.CarryFlag)
            {
                value += 1;
            }

            //Check CarryFlag
            if (value >= GlobalVariables.EightBitBoundary)
            {
                value = (ushort) (value % GlobalVariables.EightBitBoundary);
                MicroController.Memory.CarryFlag = true;
            }
            else
            {
                MicroController.Memory.CarryFlag = false;
            }

            //SubCommand saving result
            SubCommandSaveResult(destinationBit, (byte) address, (byte) value);
        }

        /// <summary>
        /// Rotate Right f through Carry
        /// </summary>
        public static void Command_RRF(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            int tempValue = MicroController.Memory.Register[address].Value;

            //Rotate
            UInt16 value = (ushort)(MicroController.Memory.Register[address].Value >> 1);

            if (MicroController.Memory.CarryFlag)
            {
                value += 0x80;
            }

            //Check CarryFlag
            if ((tempValue & 0b01) != 0)
            {
                value = (ushort)(value % GlobalVariables.EightBitBoundary);
                MicroController.Memory.CarryFlag = true;
            }
            else
            {
                MicroController.Memory.CarryFlag = false;
            }

            //SubCommand saving result
            SubCommandSaveResult(destinationBit, (byte) address, (byte) value);
        }

        /// <summary>
        /// Subtract W from f
        /// </summary>
        public static void Command_SUBWF(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            //Add
            int result = MicroController.Memory.Register[address].Value - MicroController.WRegister.Value;

            //Check CarryFlag
            if (result < 0)
            {
                result = (result % GlobalVariables.EightBitBoundary);
                MicroController.Memory.CarryFlag = false;
            }
            else
            {
                MicroController.Memory.CarryFlag = true;
            }

            //Check DigitCarry
            int digitResult = MicroController.Memory.Register[address].LowerNipple - MicroController.WRegister.LowerNipple;
            MicroController.Memory.DigitCarryFlag = !(digitResult < 0);

            //SubCommand saving result
            SubCommandSaveResult(destinationBit, (byte) address, (byte) result);
            SubCommandUpdateZeroFlag((byte) result);
        }

        /// <summary>
        /// Swap nibbles in f
        /// </summary>
        public static void Command_SWAPF(int command) //TODO result up similar functions
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            //Swap
            UInt16 UpperNippleToLower = (ushort)(MicroController.Memory.Register[address].Value >> 4);
            UInt16 newValue = (ushort)(MicroController.Memory.Register[address].Value << 4);
            newValue += UpperNippleToLower;
            newValue %= GlobalVariables.EightBitBoundary;

            //Save //TODO SubCommand
            if (destinationBit) 
                { MicroController.Memory.Register[address].Value = (byte) newValue; }
            else 
                { MicroController.WRegister.Value = (byte) newValue; }
        }

        /// <summary>
        /// Exclusive OR W with f
        /// </summary>
        public static void Command_XORWF(int command)
        {
            bool destinationBit = ExtractDestinationFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            //Add
            int result = MicroController.WRegister.Value ^ MicroController.Memory.Register[address].Value;

            //Save //TODO SubCommand
            if (destinationBit) { MicroController.Memory.Register[address].Value = (byte) result; }
            else { MicroController.WRegister.Value = (byte) result; }
        }

        #endregion

        #region BIT-ORIENTED FILE REGISTER OPERATIONS

        /// <summary>
        /// Bit Clear f
        /// </summary>
        public static void Command_BCF(int command)
        {
            byte bitPosition = ExtractBitPositionFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            MicroController.Memory.Register[address].SetBit(bitPosition, false);
        }

        /// <summary>
        /// Bit Set f
        /// </summary>
        public static void Command_BSF(int command)
        {
            byte bitPosition = ExtractBitPositionFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            MicroController.Memory.Register[address].SetBit(bitPosition, true);
        }

        /// <summary>
        /// Bit Test f, Skip if Clear
        /// </summary>
        public static void Command_BTFSC(int command)
        {
            byte bitPosition = ExtractBitPositionFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            if (!MicroController.Memory.Register[address].GetBit(bitPosition))
            {
                MicroController.Status.TimerZyklusHandled = false;
                MicroController.Memory.ProgramCounter++;
            }
        }

        /// <summary>
        /// Bit Test f, Skip if Set
        /// </summary>
        public static void Command_BTFSS(int command)
        {
            byte bitPosition = ExtractBitPositionFromCommand(command);
            int address = ExtractAddressFromCommand(command);

            if (MicroController.Memory.Register[address].GetBit(bitPosition))
            {
                MicroController.Status.TimerZyklusHandled = false;
                MicroController.Memory.ProgramCounter++;
            }
        }

        #endregion

        #region LITERAL AND CONTROL OPERATIONS

        /// <summary>
        /// Add literal and W
        /// </summary>
        public static void Command_ADDLW(int command)
        {
            byte literal = ExtractLiteralFromCommand(command);

            //Add
            int result = MicroController.WRegister.Value + literal;

            //Check Carry
            if (result >= GlobalVariables.EightBitBoundary)
            {
                result = (result % GlobalVariables.EightBitBoundary);
                MicroController.Memory.CarryFlag = true;
            }
            else
            { MicroController.Memory.CarryFlag = false; }

            //Check DigitCarry
            int digitResult = MicroController.WRegister.LowerNipple + (literal&0b0000_1111);
            MicroController.Memory.DigitCarryFlag = (digitResult >= GlobalVariables.FourBitBoundary);

            SubCommandUpdateZeroFlag((byte) result);
            //TODO SubCommand
            MicroController.WRegister.Value = (byte)result;
        }

        /// <summary>
        /// AND literal with W
        /// </summary>
        public static void Command_ANDLW(int command)
        {
            byte literal = ExtractLiteralFromCommand(command);

            //AND
            int result = MicroController.WRegister.Value & literal;

            SubCommandUpdateZeroFlag((byte) result);
            //TODO SubCommand
            MicroController.WRegister.Value = (byte)result;
        }

        /// <summary>
        /// Call subroutine
        /// </summary>
        public static void Command_CALL(int command)
        {
            int res = command & 0b111_1111_1111;

            //Mask lower & upper 3 bits and shift right by one byte
            int PCLATH = (MicroController.Memory.Register[0x0A].Value & 0b0001_1000) << 8;
            res |= PCLATH;

            MicroController.Stack[MicroController.StackPointer] = MicroController.Memory.ProgramCounter; //13Bit keine Maskierung
            MicroController.StackPointer++;
            MicroController.Status.TimerZyklusHandled = false;
            MicroController.Memory.ProgramCounter = res;
        }

        /// <summary>
        /// Clear Watchdog Timer
        /// </summary>
        public static void Command_CLRWDT()
        {
            //Wenn PreScaler WDT zugewiesen => PreScaler reset
            if (MicroController.Memory.Timer0PrescalerEnabledFlag)
                {  MicroController.Memory.PreScaler = 0; }

            MicroController.Memory.TimeOutFlag = true;
            MicroController.Memory.PowerDownFlag = true;
            MicroController.Status.WatchDogTicks = 0;
        }

        /// <summary>
        /// Go to address
        /// </summary>
        public static void Command_GOTO(int command)
        {
            int res = command & 0b111_1111_1111;

            //Mask lower & upper 3 bits and shift right by one byte
            int PCLATH = (MicroController.Memory.Register[0x0A].Value & 0b0001_1000) << 8;
            res |= PCLATH;

            MicroController.Status.TimerZyklusHandled = false;
            MicroController.Memory.ProgramCounter = res;
        }

        /// <summary>
        /// Inclusive OR literal with W
        /// </summary>
        public static void Command_IORLW(int command)
        {
            byte literal = ExtractLiteralFromCommand(command);

            //Add
            int result = MicroController.WRegister.Value | literal;

            //SubCommand Handling ZF and result saving
            SubCommandUpdateZeroFlag((byte) result);
            SubCommandSaveResult(false, 0, (byte) result);
        }

        /// <summary>
        /// Move literal to W
        /// </summary>
        public static void Command_MOVLW(int command)
        {
            byte literal = ExtractLiteralFromCommand(command);
            SubCommandSaveResult(false, 0, literal);
        }

        /// <summary>
        /// Return from interrupt
        /// </summary>
        public static void Command_RETFIE()
        {
            MicroController.StackPointer--;
            MicroController.Status.TimerZyklusHandled = false;
            MicroController.Memory.ProgramCounter = MicroController.Stack[MicroController.StackPointer];
            MicroController.Memory.GlobalInterruptEnabledFlag = true;
        }

        /// <summary>
        /// Return with literal in W
        /// </summary>
        public static void Command_RETLW(int command)
        {
            MicroController.WRegister.Value = ExtractLiteralFromCommand(command);

            MicroController.StackPointer--;
            MicroController.Status.TimerZyklusHandled = false;
            MicroController.Memory.ProgramCounter = MicroController.Stack[MicroController.StackPointer];

            //INFO: PC hier entfernt, da auf Stack schon die neue Adresse steht
        }

        /// <summary>
        /// Return from Subroutine
        /// </summary>
        public static void Command_RETURN()
        {
            MicroController.StackPointer--;

            MicroController.Status.TimerZyklusHandled = false;
            MicroController.Memory.ProgramCounter = MicroController.Stack[MicroController.StackPointer];
            //INFO: PC hier entfernt, da auf Stack schon die neue Adresse steht
        }

        /// <summary>
        /// Go into standby mode
        /// </summary>
        public static void Command_SLEEP()
        {
            //Wenn PreScaler WDT zugewiesen => PreScaler reset
            if(MicroController.Memory.Timer0PrescalerEnabledFlag)
                { MicroController.Memory.PreScaler = 0; }

            //Only Virtual
            MicroController.Memory.PowerDownFlag = false;
            MicroController.Memory.TimeOutFlag = true;
            MicroController.Status.WatchDogTicks = 0;

            //Set Sleep
            MicroController.Status.InSleep = true;
        }

        /// <summary>
        /// Subtract W from literal
        /// </summary>
        public static void Command_SUBLW(int command)
        {
            byte literal = ExtractLiteralFromCommand(command);

            //Add
            int result = literal - MicroController.WRegister.Value;

            //Check Carry
            if (result < 0)
            {
                result = (result % GlobalVariables.EightBitBoundary);
                MicroController.Memory.CarryFlag = false;
            }
            else
            { MicroController.Memory.CarryFlag = true; }

            //Check DigitCarry
            int digitResult = MicroController.WRegister.LowerNipple - (literal & 0b0000_1111);
            MicroController.Memory.DigitCarryFlag = (digitResult < GlobalVariables.FourBitBoundary);

            SubCommandUpdateZeroFlag((byte) result);
            //TODO SubCommand
            MicroController.WRegister.Value = (byte)result;
        }

        /// <summary>
        /// Exclusive OR literal with W
        /// </summary>
        public static void Command_XORLW(int command)
        {
            byte literal = ExtractLiteralFromCommand(command);

            //Add
            int result = MicroController.WRegister.Value ^ literal;

            SubCommandUpdateZeroFlag((byte) result);
            //TODO SubCommand
            MicroController.WRegister.Value = (byte)result;
        }

        #endregion

        #region Helper



        // Check Bit on Position 7. (It's the 8th. bit)
        private static bool ExtractDestinationFromCommand(int command) => (command & (1 << 7)) != 0;

        private static int ExtractAddressFromCommand(int command)
        {
            int address = command & 0x07F;

            if (address == 0x00 || address == 0x80)
            {
                return MicroController.Memory.Register[0x04].Value; //Address of indirect register
            }


            if (MicroController.Memory.DirectRegisterBankSelect0Flag) { address += 0x80; }

            return address;
        }

        private static byte ExtractLiteralFromCommand(int command) => (byte)(command & 0x0FF);

        #endregion

        private static byte ExtractBitPositionFromCommand(int command)
        {
            int bitPosition = command & 0x380;
            bitPosition >>= 7;
            
            return (byte) bitPosition;
        }
    }
}
