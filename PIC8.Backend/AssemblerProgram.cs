﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Windows.Data;

namespace PIC8
{
    public class AssemblerProgram
    {
        private static readonly string regexTextLinePattern = @"^[ ]*([\d]{5})(.*)$";
        private static readonly string regexCommandLinePattern = @"^([\da-fA-F]{4}) ([\da-fA-F]{4})[ ]*([\d]{5})(.*)$";

        public ObservableCollection<CommandLine> Source { get; set; }

        private object _linesLock = new object();

        public AssemblerProgram()
        {
            Source = new ObservableCollection<CommandLine>();
            BindingOperations.EnableCollectionSynchronization(Source, _linesLock); //Async Collection Change
        }

        public void Reset() => Source.Clear();

        public void Load(string FilePath) 
        {
            if (!File.Exists(FilePath))
                { throw new FileNotFoundException{Source = FilePath}; }

            // Start reading and parsing
            using (StreamReader file = new StreamReader(FilePath))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    CommandLine programLine = ParseLine(regexTextLinePattern, regexCommandLinePattern, line);

                    if (programLine != null)
                    {
                        Source.Add(programLine);
                    }
                }
            }

            Source.First(c => c.IsExecutable).IsCurrentExecution = true;
        }

        private static CommandLine ParseLine(string regexTextLinePattern, string regexCommandLinePattern, string line)
        {
            CommandLine programLine;

            // Match TextLine
            Match result = Regex.Match(line, regexTextLinePattern);
            if (result.Success)
            {
                string lineNumberStr = result.Groups[1].Value;
                int.TryParse(lineNumberStr, out int lineNumber);
                programLine = new CommandLine { Text = line, LineNumber = lineNumber, MemoryAddress = int.MaxValue};
            }
            else
            {
                // Match ProgramCodeLine
                result = Regex.Match(line, regexCommandLinePattern);
                if (result.Success)
                {
                    //MemAdr
                    string memAdrStr = result.Groups[1].Value;
                    int memAdr = Convert.ToInt32(memAdrStr, 16);

                    //Command
                    string commStr = result.Groups[2].Value;
                    int comm = Convert.ToInt32(commStr, 16);

                    //LineNumber
                    string lineNumberStr = result.Groups[3].Value;
                    int.TryParse(lineNumberStr, out int lineNumber);

                    //Create
                    programLine = new CommandLine{ Text = line, MemoryAddress = memAdr, Command = comm, LineNumber = lineNumber, IsExecutable = true };
                }
                else
                {
                    programLine = new CommandLine { Text = line, LineNumber = 0, MemoryAddress = int.MaxValue};
                }
            }

            return programLine;
        }
    }
}
