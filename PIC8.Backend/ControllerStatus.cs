﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using PIC8.Backend.Annotations;

namespace PIC8.Backend
{
    public class ControllerStatus : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            { PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName)); }



        public ControllerStatus()
        {
            ResetRunTime();
        }



        private bool _isRunning;
        public bool IsRunning
        {
            get => _isRunning;
            set
            {
                _isRunning = value; 
                OnPropertyChanged();
                OnPropertyChanged(nameof(StartButtonEnabled));
                OnPropertyChanged(nameof(PauseButtonEnabled));
                OnPropertyChanged(nameof(NextButtonEnabled));
                OnPropertyChanged(nameof(LoadButtonEnabled));
                OnPropertyChanged(nameof(TimeOutSliderEnabled));
                OnPropertyChanged(nameof(FrequenzySliderEnabled));
                OnPropertyChanged(nameof(WatchDogToggleSwitchEnabled));
            }
        }



        private bool _isProgramLoaded;
        public bool IsProgramLoaded
        {
            get => _isProgramLoaded;
            set
            {
                _isProgramLoaded = value; 
                OnPropertyChanged();
                OnPropertyChanged(nameof(StartButtonEnabled));
                OnPropertyChanged(nameof(NextButtonEnabled));
                OnPropertyChanged(nameof(StopButtonEnabled));
            }
        }



        private bool _isReset;
        public bool IsReset
        {
            get => _isReset;
            set
            {
                _isReset = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(LoadButtonEnabled));
                OnPropertyChanged(nameof(StopButtonEnabled));
                OnPropertyChanged(nameof(FrequenzySliderEnabled));
                OnPropertyChanged(nameof(WatchDogToggleSwitchEnabled));
            }
        }



        public bool TimerZyklusHandled {  get; internal set; }



        private bool _isWatchDogEnabled;
        public bool IsWatchDogEnabled
        {
            get => _isWatchDogEnabled;
            set
            {
                _isWatchDogEnabled = value;
                OnPropertyChanged();
            }
        }
        


        private int _watchDogTicks;
        public int WatchDogTicks
        {
            get => _watchDogTicks;
            set
            {
                _watchDogTicks = (_watchDogTicks == 18000) ? 0 : value;
                OnPropertyChanged();
            }
        }



        private static int _timeOut;
        public int TimeOut
        {
            get => _timeOut;
            set
            {
                _timeOut = value;
                OnPropertyChanged();
            }
        }



        private int _frequenz;
        public int Frequenz // Value between 1 and 10 (Mhz), Default 4
        {
            get => _frequenz;
            set
            {
                _frequenz = value;
                OnPropertyChanged();
            } 
        }



        private TimeSpan _runTime;
        public TimeSpan RunTime
        {
            get => _runTime;
        }



        private bool _inSleep;
        public bool InSleep
        {
            get => _inSleep;
            set
            {
                _inSleep = value;
                OnPropertyChanged();
            }
        }



        private bool _timeOutHandleRequired;
        public bool TimeOutHandleRequired
        {
            get => _timeOutHandleRequired;
            set
            {
                _timeOutHandleRequired = value;
                OnPropertyChanged();
            }
        }



        public void IncreaseRunTime()
        {
            _runTime = TimeSpan.FromTicks(_runTime.Ticks + (40 / Frequenz));
            OnPropertyChanged(nameof(RunTime));
        }



        internal void ResetRunTime()
        {
            _runTime = new TimeSpan(0);
            OnPropertyChanged(nameof(RunTime));
        }



        #region UI-Status
        public bool StartButtonEnabled => !IsRunning && IsProgramLoaded;

        public bool PauseButtonEnabled => IsRunning;

        public bool NextButtonEnabled => !IsRunning && IsProgramLoaded;

        public bool LoadButtonEnabled => !IsRunning && IsReset;

        public bool StopButtonEnabled => IsProgramLoaded && !IsReset;

        public bool TimeOutSliderEnabled => !IsRunning;

        public bool FrequenzySliderEnabled => !IsRunning && IsReset;

        public bool WatchDogToggleSwitchEnabled => !IsRunning && IsReset;
        #endregion
    }
}
