﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using PIC8.Backend;

namespace PIC8
{
    public static class MicroController
    {
        public static event EventHandler RegisterReset;

        private static void OnRegisterReset() => RegisterReset?.Invoke(null, EventArgs.Empty);


        public static ControllerStatus Status = new ControllerStatus { IsReset = true, TimerZyklusHandled = true, TimeOut = 500, Frequenz = 4};
        public static Memory Memory = new Memory();
        public static AssemblerProgram AssemblerProgram = new AssemblerProgram();
        public static Register WRegister = new Register();
        public static ObservableCollection<int> Stack = new ObservableCollection<int> {0,0,0,0,0,0,0,0};
        private static object _stackLock = new object();
        private static Task _executionTask;
        private static CancellationTokenSource CancellationTokenSource { get; set; }

        private static bool _latchPortB0;
        private static bool _latchPortB4;
        private static bool _latchPortB5;
        private static bool _latchPortB6;
        private static bool _latchPortB7;

        private static bool _interrupt;



        private static int _stackPointer;

        public static int StackPointer
        {
            get => _stackPointer;
            set
            {
                _stackPointer = value;
                if (_stackPointer < 0) { _stackPointer = 7; }
                _stackPointer &= 0b111;
            }
        }



        static MicroController()
        {
            BindingOperations.EnableCollectionSynchronization(Stack, _stackLock);
        }



        private static async Task ExecutionMethod()
        {
            while (!CancellationTokenSource.IsCancellationRequested)
            {
                NextInstruction();
                await Task.Delay(Status.TimeOut);
            }
        }



        public static void LoadProgram(string ofdFileName)
        {
            AssemblerProgram.Reset();
            AssemblerProgram.Load(ofdFileName);

            Status.IsProgramLoaded = true;
            Memory.ProgramCounter = 0;
        }



        public static void NextInstruction()
        {
            if (Status.IsReset)
            {
                //Preinit Latch
                _latchPortB0 = Memory.DataPortB0;
                _latchPortB4 = Memory.DataPortB4;
                _latchPortB5 = Memory.DataPortB5;
                _latchPortB6 = Memory.DataPortB6;
                _latchPortB7 = Memory.DataPortB7;
            }

            if (!Status.IsRunning)
            {
                //Do not change isReset inside Thread, it is done outside
                Status.IsReset = false;
            } 

            



            //Set Interrupts even dough GIE blocks them (documentation page 17)

            //PortB4-7
            if ((Memory.TrisPortB4 == true && (Memory.DataPortB4 ^ _latchPortB4))
                || (Memory.TrisPortB5 == true && (Memory.DataPortB5 ^ _latchPortB5))
                || (Memory.TrisPortB6 == true && (Memory.DataPortB6 ^ _latchPortB6))
                || (Memory.TrisPortB7 == true && (Memory.DataPortB7 ^ _latchPortB7)))
            {
                Memory.RPChangeInterruptFlag = true;
            }


            //PortB0
            if ((Memory.TrisPortB0 == true && //Port Eingang
                 (Memory.DataPortB0 ^ _latchPortB0 && //Flanke?
                  Memory.DataPortB0 == Memory.InterruptEdgeSelectFlag))) //Richtige Flanke
            {
                Memory.RB0InterruptFlag = true;
            }


            _latchPortB0 = Memory.DataPortB0;
            _latchPortB4 = Memory.DataPortB4;
            _latchPortB5 = Memory.DataPortB5;
            _latchPortB6 = Memory.DataPortB6;
            _latchPortB7 = Memory.DataPortB7;
            _interrupt = false;

            /*Interrupt-Timer0*/
            if (Memory.Timer0OverflowInterruptEnabledFlag && Memory.Timer0OverflowInterruptFlag)
                { _interrupt = true; }

            /*Interrupt-PortB0*/
            if (Memory.RB0InterruptEnabledFlag && Memory.RB0InterruptFlag)
                { _interrupt = true; }

            /*Interrupt-PortB4-7*/
            if (Memory.RBChangeInterruptEnabledFlag && Memory.RPChangeInterruptFlag)
                { _interrupt = true; }

            /*Interrupt-EEProm*/
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (Memory.EEWriteCompleteInterruptEnabledFlag && false/*&& Memory.EEWriteCompleteInterruptFlag*/) //TODO
                { _interrupt = true; }



            if (_interrupt)
            {
                if (Status.InSleep) //is powered down
                {
                    Status.InSleep = false;
                    // disable power down and execute fetched command, 
                    // next instruction will jump to interrupt vector, if GIE is enabled
                }
                else if (Memory.GlobalInterruptEnabledFlag)
                {
                    Stack[StackPointer] = Memory.ProgramCounter;
                    StackPointer++;
                    Memory.ProgramCounter = 0x04;
                    Memory.GlobalInterruptEnabledFlag = false;
                }
            }







            //WDT Enabled + Sleep => Reset
            if (Status.IsWatchDogEnabled && Status.TimeOutHandleRequired)
            {
                if (!Status.InSleep) //Falls läuft => Reset
                    { Memory.ProgramCounter = 0; }


                Status.InSleep = false;
                Status.TimeOutHandleRequired = false;


                //Set Flags
                Memory.TimeOutFlag = false;
                Memory.PowerDownFlag = false;
            }


            if (Status.InSleep)
            {
                Status.TimerZyklusHandled = false; //Handle Timeout
                Memory.ProgramCounter = Memory.ProgramCounter;
                return;
            }




            //Timer an 
            if (Memory.Timer0ClockSourceSelectFlag)
            {
                if (Memory.DataPortA4 ^ Memory.Timer0SourceEdgeSelectFlag) //Check for Flank, type determined by T0SE
                {
                    Memory.Timer0SourceEdgeSelectFlag ^= true; //Invert

                    if (Memory.Timer0PrescalerEnabledFlag)
                    {
                        //Disabled
                        Memory.Register[0x01].Value++;
                    }
                    else
                    {
                        //Enabled
                        Memory.PreScalerTick(Memory.Timer0Rate);

                        //Auf Overflow prüfen
                        if (Memory.PreScaler == 0 && Memory.Register[0x01].Value == 0)
                        {
                            Memory.Timer0OverflowInterruptFlag = true;
                        }
                    }
                }
            }






            CommandLine cmd = AssemblerProgram.Source.Single(pm => pm.MemoryAddress == Memory.ProgramCounter);
            if (cmd.IsBreakPoint)
            {
                CancellationTokenSource?.Cancel();
                cmd.IsBreakPoint = false;
                Status.IsRunning = false;
                return;
            }

            Instructions.ParseCommand(cmd);

            //AssemblerProgram.Source.Single(pm => pm.MemoryAddress == Memory.ProgramCounter).IsCurrentExecution = true;
        }



        public static void PauseExecution()
        {
            StopTask();
        }



        public static async void StartExecution()
        {
            Status.IsRunning = true;
            Status.IsReset = false;
            CancellationTokenSource = new CancellationTokenSource();
            _executionTask = Task.Run(ExecutionMethod, CancellationTokenSource.Token);
            await _executionTask;
        }



        public static void StopExecution()
        {
            StopTask();

            AssemblerProgram.Source.Single(c => c.MemoryAddress == Memory.ProgramCounter).IsCurrentExecution = false;

            Memory.Reset(); //Resets ProgramCounter
            WRegister.Value = 0x00;

            for (int i = 0; i < 8; i++) 
                { Stack[i] = 0; }
            StackPointer = 0;
            
            AssemblerProgram.Source.First(c => c.IsExecutable).IsCurrentExecution = true;
            
            Status.ResetRunTime();

            Status.InSleep = false;
            Status.TimeOutHandleRequired = false;

            Status.WatchDogTicks = 0;
            Memory.PreScaler = 0;

            Status.IsReset = true;
            OnRegisterReset();
        }



        private static async void StopTask()
        {
            if (!Status.IsRunning) return;

            CancellationTokenSource?.Cancel();
            await Task.Delay(Status.TimeOut + 100);

            Status.IsRunning = false;
        }
    }
}
