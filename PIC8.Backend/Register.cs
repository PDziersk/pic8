﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using PIC8.Backend.Annotations;

namespace PIC8
{
    public class Register : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            { PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName)); }



        private byte _value;
        private bool _marked;

        #region Properties

        public byte Address { get; }
        
        public byte Value
        {
            get => _value;
            set
            {
                _value = value;
                OnPropertyChanged();
                OnPropertyChanged("ValueAsHexString");
                OnPropertyChanged("ValueAsBinaryString");
                OnPropertyChanged("ToolTipString");
                OnPropertyChanged("LowerNipple");
                OnPropertyChanged("UpperNipple");
            }
        }

        public byte LowerNipple => (byte)(_value & 0b0000_1111);

        public byte UpperNipple => (byte)(_value & 0b1111_0000 >> 4);

        public bool Marked
        {
            get => _marked;
            set
            {
                _marked = value;
                OnPropertyChanged();
            }
        }

        public string ValueAsHexString => $"0x{Convert.ToString(Value, 16).ToUpper().PadLeft(2, '0')}";

        public string ValueAsBinaryString => $"{Convert.ToString(Value, 2).ToUpper().PadLeft(8, '0')}";

        public string AddressAsHexString => $"0x{Convert.ToString(Address, 16).ToUpper().PadLeft(2, '0')}";

        public string ToolTipString => $"Address: {AddressAsHexString}\nBinary-Value: {ValueAsBinaryString}";

        #endregion

        public Register(byte address)
        {
            try
            {
                Address = address;
                Marked = false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public Register()
        {
            Marked = false;
        }

        public bool GetBit(int position) => (Value & (1 << position)) != 0;
        
        public void SetBit(int position, bool newValue)
        {
            if (newValue) 
                { Value |= (byte) (1 << position);  }
            else 
                { Value &= (byte) (~(1 << position)); }
        }

        public void ClearValue() => Value = 0;
        
        public void PutMask(byte mask) => Value &= mask;
    }
}
