﻿using System;

namespace PIC8.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            TestRegister();
            //TestAssemblerProgram();
            TestRegister();
            TestCommands();
            TestMemory();

            Console.Read();
        }
        
        private static void TestRegister()
        {
            Console.WriteLine("____________________Test Register____________________");
            //Test0
            Register reg = new Register();

            Console.Write("Test Get-/SetValue: ");
            reg.Value = 0x20;
            if(reg.Value == 0x20)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");

            Console.Write("Test ClearValue: ");
            reg.ClearValue();
            if (reg.Value == 0x00)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");

            Console.Write("Test Get-/SetBit: ");
            reg.SetBit(1,true);
            if (reg.GetBit(1))
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
        }

        private static void TestAssemblerProgram()
        {
            Console.WriteLine("____________________Test AssemblerLoad____________________");

            //TestLoad
            MicroController.AssemblerProgram.Load(@"D:\OneDrive\OneDrive - bwedu\Studium\2018_RM\4.Semester\SystemnahesProgrammieren\Testprogramme_PicSim_2019_04_29\TPicSim1.LST");

            //Get ProgramCode Info
            foreach (CommandLine cmd in MicroController.AssemblerProgram.Source)
            {
                if(typeof(CommandLine) == cmd.GetType()) 
                    Console.WriteLine("LineNumber: " + cmd.LineNumber + ", MemAddr: " + cmd.MemoryAddress + ", Command: " + cmd.Command + ", Text: " + cmd.Text);
                 else
                    Console.WriteLine("LineNumber: " + cmd.LineNumber + ",  Text: " + cmd.Text );
            }
        }

        private static void TestCommands()
        {
            Console.WriteLine("____________________Test Commands____________________");

            int pc_temp = 0;

            Instructions.ParseCommand(new CommandLine {Command = 1792}); // ADDWF Function

            #region Test BYTE-ORIENTED FILE REGISTER OPERATIONS
            #region   TEST ADDWF to W
            Console.Write("Test ADDWF to W: ");
            MicroController.Memory.Register[0x10].Value = 0x01;
            MicroController.WRegister.Value = 0x0A;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0111_0001_0000 });
            if (MicroController.WRegister.Value == 0x0B)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST ADDWF to F
            Console.Write("Test ADDWF to F: ");
            MicroController.Memory.Register[0x10].ClearValue();
            MicroController.WRegister.Value = (0x0A);
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0111_1001_0000 });
            if (MicroController.Memory.Register[0x10].Value == 0x0A)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST ADDWF WITH OVERFLOW
            Console.Write("Test ADDWF WITH OVERFLOW: ");
            MicroController.Memory.Register[0x10].Value = 0xFF;
            MicroController.WRegister.Value = (0xFF);
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0111_1001_0000 });
            if (MicroController.Memory.Register[0x10].Value >= 0x00 && MicroController.Memory.CarryFlag)
            {
                MicroController.WRegister.Value = (0x01);
                Instructions.ParseCommand(new CommandLine { Command = 0b00_0111_1001_0000 });
                if (!MicroController.Memory.CarryFlag)
                {
                    Console.Write("SUCCESS\n\n");
                }
                else
                    Console.Write("FAILED\n\n");
            }          
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST ADDWF WITH OVERFLOW
            Console.Write("Test ADDWF WITH ZERO: ");
            MicroController.Memory.Register[0x10].Value = 0xFF;
            MicroController.WRegister.Value = (0x01);
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0111_1001_0000 });
            if (MicroController.Memory.Register[0x10].Value == 0x00 && MicroController.Memory.ZeroFlag)
            {
                Instructions.ParseCommand(new CommandLine { Command = 0b00_0111_1001_0000 });
                if (!MicroController.Memory.ZeroFlag)
                {
                    Console.Write("SUCCESS\n\n");
                }
                else
                    Console.Write("FAILED\n\n");
            }
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST ANDWF to W
            Console.Write("Test ANDWF to W: ");
            MicroController.Memory.Register[0x10].Value = 0x03;
            MicroController.WRegister.Value = (0x05);
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0101_0001_0000 });
            if (MicroController.WRegister.Value == 0x01)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST CLRF
            Console.Write("Test CLRF: ");
            MicroController.Memory.Register[0x10].Value = 0xFF;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0001_1001_0000 }); 
            if (MicroController.Memory.Register[0x10].Value == 0x00)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST CLRW
            Console.Write("Test CLRW: ");
            MicroController.WRegister.Value = (0xFF);
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0001_0000_0000 }); // CLRW
            if(MicroController.WRegister.Value== 0x00)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST COMF
            Console.Write("Test COMF: ");
            MicroController.Memory.Register[0x10].Value = 0x01;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1001_0001_0000 }); // COMF
            if (MicroController.WRegister.Value == 0xFE)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST DECF
            Console.Write("Test DECF: ");
            MicroController.Memory.Register[0x10].Value = 0xFF;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0011_0001_0000 }); // CLRW
            if (MicroController.WRegister.Value== 0xFE)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region TODO DECSFZ
            Console.Write("Test DECSFZ: ");
            pc_temp = MicroController.Memory.ProgramCounter;
            MicroController.Memory.Register[0x10].Value = 0xFF;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1011_0001_0000 });
            if (MicroController.WRegister.Value == 0xFE && MicroController.Memory.ProgramCounter == pc_temp + 1)
            {
                Console.Write("SUCCESS\n\n");
            }
            else
                Console.Write("FAILED\n\n");

            Console.Write("Test DECSFZ With Skip: ");
            pc_temp = MicroController.Memory.ProgramCounter;
            MicroController.Memory.Register[0x10].Value = 0x01;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1011_0001_0000 });
            if (MicroController.WRegister.Value == 0x00 && MicroController.Memory.ProgramCounter == pc_temp + 2)
            {
                Console.Write("SUCCESS\n\n");
            }
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST INCF
            Console.Write("Test INCF: ");
            MicroController.Memory.Register[0x10].Value = 0x00;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1010_0001_0000 });
            if (MicroController.WRegister.Value== 0x01)
                Console.Write("SUCCESS\n\n"); 
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region TODO INCSFZ
            Console.Write("Test INCSFZ: ");
            pc_temp = MicroController.Memory.ProgramCounter;
            MicroController.Memory.Register[0x10].Value = 0x00;     
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1111_0001_0000 }); 
            if (MicroController.WRegister.Value == 0x01 && MicroController.Memory.ProgramCounter == pc_temp + 1)
            {
                Console.Write("SUCCESS\n\n");
            }else
            Console.Write("FAILED\n\n");

            Console.Write("Test INCSFZ With Skip: ");
            pc_temp = MicroController.Memory.ProgramCounter;
            MicroController.Memory.Register[0x10].Value = 0xFF;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1111_0001_0000 });
            if (MicroController.WRegister.Value == 0x00 && MicroController.Memory.ProgramCounter == pc_temp + 2)
            {
                Console.Write("SUCCESS\n\n");
            }
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region TEST IORWF to W
            Console.Write("Test IOR to W: ");
            MicroController.Memory.Register[0x10].Value = 0x03;
            MicroController.WRegister.Value = (0x04);
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0100_0001_0000 });
            if (MicroController.WRegister.Value== 0x07)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region TEST MOVF to f
            Console.Write("Test MOVF to f (NoZero): ");
            MicroController.Memory.Register[0x10].Value = 0x03;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1000_1001_0000 });
            if (MicroController.Memory.Register[0x10].Value == 0x03 && !MicroController.Memory.ZeroFlag)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");

            Console.Write("Test MOVF to W (Zero): ");
            MicroController.WRegister.Value = (0x04);
            MicroController.Memory.Register[0x10].Value = 0x00;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1000_0001_0000 });
            if (MicroController.WRegister.Value== 0x00 && MicroController.Memory.ZeroFlag)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST MOVWF
            Console.Write("Test MOVWF to f: ");
            MicroController.WRegister.Value = (0x04);
            MicroController.Memory.Register[0x10].Value = 0x03;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0000_1001_0000 });
            if (MicroController.Memory.Register[0x10].Value == 0x04)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST NOP
            Console.Write("Test NOP: ");
            pc_temp = MicroController.Memory.ProgramCounter;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0000_0110_0000 });
            if (MicroController.Memory.ProgramCounter == pc_temp+1)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region TEST RLF
            Console.Write("Test RLF to f: ");
            MicroController.Memory.CarryFlag = false;
            MicroController.Memory.Register[0x10].Value = 0x01;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1101_1001_0000 });
            if (MicroController.Memory.Register[0x10].Value == 0x02)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region  Test RLF with Carry
            Console.Write("Test RLF with Carry: ");
            MicroController.Memory.CarryFlag = true;
            MicroController.Memory.Register[0x10].Value = 0xFF;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1101_1001_0000 });
            if (MicroController.Memory.Register[0x10].Value == 0xFF)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region  Test RLF with OverFlow
            Console.Write("Test RLF with OverFlow: ");
            MicroController.Memory.CarryFlag = false;
            MicroController.Memory.Register[0x10].Value = 0xFF;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1101_1001_0000 });
            if (MicroController.Memory.Register[0x10].Value == 0xFE && MicroController.Memory.CarryFlag)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST RRF
            Console.Write("Test RRF to f: ");
            MicroController.Memory.CarryFlag = false;
            MicroController.Memory.Register[0x10].Value = 0x02;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1100_1001_0000 });
            if (MicroController.Memory.Register[0x10].Value == 0x01)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST RRF with Carry
            Console.Write("Test RRF with Carry : ");
            MicroController.Memory.CarryFlag = true;
            MicroController.Memory.Register[0x10].Value = 0x02;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1100_1001_0000 });
            if (MicroController.Memory.Register[0x10].Value == 0x81)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST RRF with OverFlow
            Console.Write("Test RRF with OverFlow: ");
            MicroController.Memory.CarryFlag = false;
            MicroController.Memory.Register[0x10].Value = 0x03;
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1100_1001_0000 });
            if (MicroController.Memory.Register[0x10].Value == 0x01 && MicroController.Memory.CarryFlag)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST SUBWF to W
            Console.Write("Test SUBWF to W: ");
            MicroController.Memory.Register[0x10].Value = 0xFF;
            MicroController.WRegister.Value = (0x01);
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0010_0001_0000 });
            if (MicroController.WRegister.Value== 0xFE)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST SUBWF with Overflow
            Console.Write("Test SUBWF with Overflow: ");
            MicroController.Memory.Register[0x10].Value = 0x0;
            MicroController.WRegister.Value = (0x01);
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0010_0001_0000 });
            if (MicroController.WRegister.Value== 0xFF)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST SWAPF to W
            Console.Write("Test SWAPF to W: ");
            MicroController.Memory.Register[0x10].Value = 0x1F;
            MicroController.WRegister.ClearValue();
            Instructions.ParseCommand(new CommandLine { Command = 0b00_1110_0001_0000 });
            if (MicroController.WRegister.Value == 0xF1)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region   TEST XORWF to W
            Console.Write("Test XORWF to W: ");
            MicroController.Memory.Register[0x10].Value = 0x1;
            MicroController.WRegister.Value = (0x2);
            Instructions.ParseCommand(new CommandLine { Command = 0b00_0110_0001_0000 });
            if (MicroController.WRegister.Value == 0x3)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion   TEST XORWF to W

            #endregion

            #region Test BIT-ORIENTED FILE REGISTER OPERATIONS
            #region   TEST BCF to W
            Console.Write("TEST BCF to W: ");
            MicroController.Memory.Register[0x10].Value = 0x99;
            Instructions.ParseCommand(new CommandLine { Command = 0b01_0011_1001_0000 });
            if (MicroController.Memory.Register[0x10].Value == 0x19)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion 

            #region   TEST BSF to W
            Console.Write("TEST BSF to W: ");
            MicroController.Memory.Register[0x10].Value = 0x19;
            Instructions.ParseCommand(new CommandLine { Command = 0b01_0111_1001_0000 });
            if (MicroController.Memory.Register[0x10].Value == 0x99)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion 

            #region  TEST BTFSC to W
            Console.Write("TEST BTFSC to W: ");
            pc_temp = MicroController.Memory.ProgramCounter;
            MicroController.Memory.Register[0x10].Value = 0x80;
            Instructions.ParseCommand(new CommandLine { Command = 0b01_1011_1001_0000 });
            if (MicroController.Memory.ProgramCounter == pc_temp + 1)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");

            Console.Write("TEST BTFSC to W (SKIP): ");
            pc_temp = MicroController.Memory.ProgramCounter;
            MicroController.Memory.Register[0x10].Value = 0x7F;
            Instructions.ParseCommand(new CommandLine { Command = 0b01_1011_1001_0000 });
            if (MicroController.Memory.ProgramCounter == pc_temp + 2)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #region  TEST BTFSS to W
            Console.Write("TEST BTFSS to W: ");
            pc_temp = MicroController.Memory.ProgramCounter;
            MicroController.Memory.Register[0x10].Value = 0x7F;
            Instructions.ParseCommand(new CommandLine { Command = 0b01_1111_1001_0000});
            if (MicroController.Memory.ProgramCounter == pc_temp + 1)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");

            Console.Write("TEST BTFSS to W(SKIP): ");
            pc_temp = MicroController.Memory.ProgramCounter;
            MicroController.Memory.Register[0x10].Value = 0x80;
            Instructions.ParseCommand(new CommandLine { Command = 0b01_1111_1001_0000});
            if (MicroController.Memory.ProgramCounter == pc_temp + 2)
                Console.Write("SUCCESS\n\n");
            else
                Console.Write("FAILED\n\n");
            #endregion

            #endregion

            #region Test LITERAL AND CONTROL OPERATIONS
            //  TEST ADDLW
            //TODO

            //  TEST ANDLW
            //TODO

            //  TEST CALL
            //TODO

            //  TEST CLRWDT
            //TODO

            //  TEST GOTO
            //TODO

            //  TEST IORLW
            //TODO

            //  TEST MOVLW
            //TODO

            //  TEST RETFIE
            //TODO

            //  TEST RETLW
            //TODO

            //  TEST RETURN
            //TODO

            //  TEST SLEEP
            //TODO

            //  TEST SUBLW
            //TODO

            //  TEST XORLW
            //TODO

            #endregion
        }

        private static void TestMemory()
        {
            Console.WriteLine("____________________Test Memory____________________");

            //TEST Memor<
            //MicroController.Memory.FillWithRandom();
            Console.WriteLine("   \t00\t01\t02\t03\t04\t05\t06\t07\t08\t09\t0A\t0B\t0C\t0D\t0E\t0F");
            Console.WriteLine(" __\t__\t__\t__\t__\t__\t__\t__\t__\t__\t__\t__\t__\t__\t__\t__\t__");
            for (int i = 0; i < 16; i++)
            {
                Console.Write(i.ToString("X2") + "|\t");
                for (int j = 0; j < 16; j++)
                {           
                    Register reg = MicroController.Memory.Register[i+j];
                    Console.Write(reg.ValueAsHexString + '\t');
                }
                Console.Write(System.Environment.NewLine);
            }

            //TEST STATUS
            Console.Write("\n\n\n");
            MicroController.Memory.Status.Value = (0xFF);
            if (MicroController.Memory.Register[0x83].Value == 0xFF)
            {
                Console.Write("SUCCESS\n\n");
            }
            else
            {
                Console.Write("FAILED\n\n");
            }

            //TEST ZERO FLAGS
            Console.Write("Test SET/CLEAR ZERO FLAG: ");
            MicroController.Memory.ZeroFlag = true;
            if (MicroController.Memory.ZeroFlag)
            {
                MicroController.Memory.ZeroFlag = false;
                if (!MicroController.Memory.ZeroFlag)
                    Console.Write("SUCCESS\n\n");
                else
                    Console.Write("FAILED\n\n");
            }
            else
                Console.Write("FAILED\n\n");

            //TEST DIGITCARRY FLAGS
            Console.Write("Test SET/CLEAR DIGITCARRY FLAG: ");
            MicroController.Memory.DigitCarryFlag = true;
            if (MicroController.Memory.DigitCarryFlag)
            {
                MicroController.Memory.DigitCarryFlag = false;
                if (!MicroController.Memory.DigitCarryFlag)
                    Console.Write("SUCCESS\n\n");
                else
                    Console.Write("FAILED\n\n");
            }
            else
                Console.Write("FAILED\n\n");

            //TEST CARRY FLAGS
            Console.Write("Test SET/CLEAR CARRY FLAG: ");
            MicroController.Memory.CarryFlag = true;
            if (MicroController.Memory.CarryFlag)
            {
                MicroController.Memory.CarryFlag = false;
                if (!MicroController.Memory.CarryFlag)
                    Console.Write("SUCCESS\n\n");
                else
                    Console.Write("FAILED\n\n");
            }
            else
                Console.Write("FAILED\n\n");

        }
    }
}
