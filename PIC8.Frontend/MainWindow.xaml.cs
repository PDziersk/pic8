﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;

namespace PIC8.Frontend
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            PCLabel.DataContext = MicroController.Memory;
            PreScalerLabel.DataContext = MicroController.Memory;
            WDTLabel.DataContext = MicroController.Status;
            WDTScalerLabel.DataContext = MicroController.Memory;
            WRegisterLabel.DataContext = MicroController.WRegister;
            StackListBox.ItemsSource = MicroController.Stack;
            RunTimeLabel.DataContext = MicroController.Status;
            

            ActionListBox.ItemsSource = MicroController.AssemblerProgram.Source;

            MicroController.RegisterReset += (o, a) => OnRegisterReset();

            MicroController.Memory.PropertyChanged += (o, a) => { Application.Current.Dispatcher.Invoke(new Action(() => OnMemoryPropertyChanged(a))); };

            OnRegisterReset();
        }



        private void OnMemoryPropertyChanged(PropertyChangedEventArgs a)
        {
            if (a.PropertyName != "ProgramCounter") return;

            if(MicroController.AssemblerProgram.Source.All(c => c.MemoryAddress != MicroController.Memory.ProgramCounter)) return;

            CommandLine cmd = MicroController.AssemblerProgram.Source.First(c => c.MemoryAddress == MicroController.Memory.ProgramCounter);
            int currentExecutionIndex = MicroController.AssemblerProgram.Source.IndexOf(cmd);

            ActionListBox.ScrollIntoView(ActionListBox.Items[currentExecutionIndex]);
        }



        private void OnRegisterReset()
        {
            #region set Datacontext vor Memory Label

            int i = 0;
            foreach (StackPanel sp in MemoryBank0StackPanel.Children)
            {
                foreach (Label label in sp.Children)
                {
                    label.DataContext = MicroController.Memory.Register[i];
                    i++;
                }
            }

            i = 0x80;
            foreach (StackPanel sp in MemoryBank1StackPanel.Children)
            {
                foreach (Label label in sp.Children)
                {
                    label.DataContext = MicroController.Memory.Register[i];
                    i++;
                }
            }

            #endregion


            StatusRegisterLabel.DataContext = MicroController.Memory.Register[0x03];
            OptionRegisterLabel.DataContext = MicroController.Memory.Register[0x81];
            InterruptRegisterLabel.DataContext = MicroController.Memory.Register[0x0B];
            
            PCLRegisterLabel.DataContext = MicroController.Memory.Register[0x02];
            PCLATHRegisterLabel.DataContext = MicroController.Memory.Register[0x8A];
            FSRRegisterLabel.DataContext = MicroController.Memory.Register[0x04];
        }



        private void MemoryLabel_OnClick(object sender, MouseButtonEventArgs e)
        {
            ((Register)((Label)sender).DataContext).Marked ^= true; //Invert
        }





        private void NextExecutionButton_OnClick(object sender, RoutedEventArgs e) => MicroController.NextInstruction();

        private void LoadButton_OnClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == true)
            {
                MicroController.LoadProgram(ofd.FileName);
            }
        }

        private void PauseExecutionButton_OnClick(object sender, RoutedEventArgs e) => MicroController.PauseExecution();

        private void StartExecutionButton_OnClick(object sender, RoutedEventArgs e) => MicroController.StartExecution();

        private void StopButton_OnClick(object sender, RoutedEventArgs e) => MicroController.StopExecution();

        private void HelpButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start($"{AppDomain.CurrentDomain.BaseDirectory}\\Docs\\Documentation.pdf");
        }
    }
}
